<table>
    <tr>
        <th>
            Cavab
        </th>
        <th>
            Status
        </th>
        <th>
            Əməliyyatlar
        </th>
    </tr>
    @foreach ($answers as $a)
        <tr>
            <td> {{ $a->answer }}</td>
            <td> {{ $a->is_true }}</td>
            <td>
                <a href="http://">Redakte &rarr;</a>
                <a href="http://" style="color:red">Sil &rarr;</a>
            </td>
        </tr>
    @endforeach
</table>
