@extends('layouts.app')

@section('header', 'Testler')
@section('content')
    <div>
        <div style="">
            <a href="/tests/create" style="padding: 5px 5px 5px 5px"><b>Sual elave et</b></a>
        </div>
        {{-- {{ dd($tests) }} --}}
        @foreach ($tests as $test)
            <div style="background-color: rgb(209, 239, 239); padding: 3px">
                <p>{{ $loop->iteration }} - {{ $test->name }}</p>

                <ol type="A">
                    @foreach ($test->answers as $answer)
                        <li>{{ $answer->answer }}</li>
                    @endforeach
                </ol>

                <div>
                    <a href="{{ route('test.edit', $test->id) }}">Redakte &rarr;</a>
                    <a href="#" style="color:red" id="delete_test" data-id="{{ $test->id }}">Sil &rarr;</a>
                </div>
                <hr>
            </div>
            <br>
        @endforeach

    </div>


    <script>
        $(document).on("click", "#delete_test", function(e) {
            e.preventDefault();
            let test_id = $(this).data("id");
            let url = "{{ route('test.delete', ':id') }}";
            url = url.replace(':id', test_id);
            $.post({
                url: url,
                success: function(data) {
                    alert("Test Silindi");
                },
                error: function(err) {
                    alert("Error");
                }
            })
        })
    </script>
@endsection
