@extends('layouts.app')
@section('title', 'Create test')
@section('header', 'Create test')
@section('content')
    <div>
        <div style="border-style:dashed; padding:10px; background-color:aliceblue">
            <form action="">
                <div>
                    <label for="">Sual:</label>
                    <input style="width: 400" type="text" id="test_name">
                    <input type="hidden" id="test_id" value="">
                </div>
                <br>

                <div>
                    <button type="submit" id="save_test">Yadda saxla</button>
                </div>
            </form>
        </div>
        <br>
        <div style="border-style:dashed; padding:10px; background-color:aliceblue">
            <form action="">
                <div>
                    <label for="">Cavab</label>
                    <input type="text" id="answer" style="width:30%">
                    <input type="checkbox" id="is_true">
                    <input type="hidden" id="answer_id" value="">
                    <button id="save_answer">+</button>
                </div>
                <br>
                <div id="answer_table">
                    <table>
                        <tr>
                            <th>
                                Cavab
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Əməliyyatlar
                            </th>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>

    <div>
        <a href="{{ route('tests') }}">Ana Sehife</a>
    </div>

    <script>
        $(document).on('click', '#save_test', function(e) {
            e.preventDefault();

            let test = document.getElementById('test_name').value;
            url = "{{ route('test.store') }}";

            $.post({
                url: url,
                data: {
                    test: test
                },
                success: function(resp) {
                    alert(resp.success);
                    $('#test_id').val(resp.test_id);
                    alert(resp.test_id);
                },
                error: function(e) {
                    alert('error');
                }
            })
        })

        $(document).on("click", "#save_answer", function(e) {
            e.preventDefault();
            let url = "{{ route('answer.store') }}"

            let data = {
                answer: document.getElementById('answer').value,
                is_true: $("#is_true").is(":checked") ? 1 : 0,
                test_id: $("#test_id").val()
            }

            console.log(data);


            $.post({
                url: url,
                data: data,
                success: function(resp) {
                    $("#answer_table").html(resp);
                }
            });
        });
    </script>
@endsection
