@extends('layouts.app')

@section('title', 'Update test')
@section('header', 'Update test')
@section('content')
    <div>
        <div style="border-style:dashed; padding:10px; background-color:aliceblue">
            <form action="">
                <div>
                    <label for="">Sual:</label>
                    <input style="width: 400" type="text" id="test_name" value="{{ $test->name }}">
                    <input type="hidden" id="test_id" value="{{ $test->id }}">
                </div>
                <br>

                <div>
                    <button type="submit" id="update_test">Yadda saxla</button>
                </div>
            </form>
        </div>
        <br>
        <div style="border-style:dashed; padding:10px; background-color:aliceblue">
            <form action="">
                <div>
                    <label for="">Cavab</label>
                    <input type="text" id="answer" style="width:30%">
                    <input type="checkbox" id="is_true" value="">
                    <input type="hidden" id="answer_id" value="">
                    <button id="update_answer">Elave et</button>
                </div>
                <br>
                <div id="answer_table">
                    <table>
                        <tr>
                            <th>
                                Cavab
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Əməliyyatlar
                            </th>
                        </tr>
                        @foreach ($answers as $answer)
                            <tr>
                                <td> {{ $answer->answer }}</td>
                                <td> {{ $answer->is_true }}</td>
                                <td>
                                    <a href="#" data-id="{{ $answer->id }}" data-answer="{{ $answer->answer }}"
                                        id="edit_answer">Redakte &rarr;</a>
                                    <a href="http://" style="color:red">Sil &rarr;</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </form>
        </div>
    </div>
    <div>
        <a href="{{ route('tests') }}">Ana Sehife</a>
    </div>

    <script>
        // UPDATE TEST
        $(document).on('click', '#update_test', function(e) {
            e.preventDefault();

            let test = document.getElementById('test_name').value;
            let test_id = document.getElementById('test_id').value;
            url = "{{ route('test.update', ':id') }}";
            url = url.replace(':id', test_id)

            console.log(url);

            $.post({
                url: url,
                data: {
                    test: test,
                },
                success: function(resp) {
                    alert(resp.success);
                    $('#test_id').val(resp.test_id);
                    alert(resp.test_id);
                },
                error: function(e) {
                    alert('error');
                }
            })
        })

        //  EDIT ANSWER
        $(document).on("click", "#edit_answer", function(e) {
            e.preventDefault();

            let answer_id = $(this).data('id');
            let answer = $(this).data('answer');

            $("#answer").val(answer);
            $("#answer_id").val(answer_id);
        });

        //  UPDATE ANSWER
        $(document).on("click", "#update_answer", function(e) {
            e.preventDefault();

            let test_id = document.getElementById('test_id').value;
            // alert(answer_id);
            alert(test_id);
            let answer_id = document.getElementById('answer_id').value;
            let answer = document.getElementById('answer').value;
            let is_true = $("#is_true").is(":checked") ? 1 : 0;

            if (answer_id != "") {

                let url = "{{ route('answer.update', ':id') }}"
                url = url.replace(':id', answer_id);

                let data = {
                    answer: answer,
                    answer_id: answer_id,
                    test_id: test_id,
                    is_true: is_true
                }
                console.log(data);

                $.post({
                    url: url,
                    data: data,
                    success: function(resp) {
                        $("#answer_table").html(resp);
                        $("#answer_id").val("");
                        $("#answer").val("");
                    }
                });
            } else {
                let url = "{{ route('answer.store') }}"

                let data = {
                    answer: answer,
                    test_id: test_id,
                    is_true: is_true
                }
                console.log(data);

                $.post({
                    url: url,
                    data: data,
                    success: function(resp) {
                        $("#answer_table").html(resp);
                    }
                });
            }
        });
    </script>
@endsection
