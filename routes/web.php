<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/tests', [TestController::class, 'index'])->name('tests');
Route::get('/tests/create', [TestController::class, 'create'])->name('test.create');
Route::post('/tests/store', [TestController::class, 'store'])->name('test.store');
Route::get('/tests/{id}/edit', [TestController::class, 'edit'])->name('test.edit');
Route::post('/tests/{id}', [TestController::class, 'update'])->name('test.update');
Route::post('/tests/{id}', [TestController::class, 'destroy'])->name('test.delete');



Route::post('/answers/store', [AnswerController::class, 'store'])->name('answer.store');
Route::post('/answers/{id}/edit', [AnswerController::class, 'edit'])->name('answer.edit');
Route::post('/answers/{id}', [AnswerController::class, 'update'])->name('answer.update');
