<?php

namespace App\Models;

use App\Models\Answer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Test extends Model
{
    use HasFactory;

    protected $table = "tests";

    protected $fillable = ['name'];


    public function answers(){
        return $this->hasMany(Answer::class, 'test_id', 'id');
    }
}
