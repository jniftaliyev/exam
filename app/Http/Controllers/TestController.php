<?php

namespace App\Http\Controllers;

use App\Http\Resources\AnswerResourse;
use App\Models\Answer;
use App\Models\Test;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::all();
        return view('tests.index', ['tests' => AnswerResourse::collection($tests)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = Test::create([
            'name' => $request->test
        ]);

        // return view('answers.create');
        return response()->json([
            'success' => 'Sual elave olundu',
            'test_id' => $test->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::find($id);

        $answers = Answer::where('test_id', $id)->get();

        return view('tests.edit', [
            'test' => $test,
            'answers' => $answers
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Test::where('id', $id)->update([
            'name' => $request->test
        ]);

        return response()->json([
            'success' => 'Test yenilendi',
            'test_id' => $id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Test::find($id)->delete();

        Answer::where('test_id', $id)->delete();

        return redirect()->route('tests');
    }
}
